﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public class Comparator : IComparator
    {
        public ComporatorResult Comare(int a, int b)
        {
            if (a > b)
                return ComporatorResult.Larger;
            else if (a < b)
                return ComporatorResult.Less;
            else
                return ComporatorResult.Equals;

        }
    }
}
