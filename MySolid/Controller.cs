﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public class Controller
    {
        private readonly IDataConsole _dataConsole;
        private readonly IGenerator<int> _generator;
        private readonly IComparator _comparator;

        private int b;

        //Принцип инверсии зависимостей
        public Controller(IDataConsole dataConsole, IGenerator<int> generator , IComparator comparator )
        {
            this._dataConsole = dataConsole;
            this._generator = generator;
            this._comparator = comparator;

            b = this._generator.Generate(0, 3);
        }

        public bool Execute()
        {
            this.ReadVariables(out var a);

            var result = this._comparator.Comare(a, b);

            this._dataConsole.Write(result.ToString());

            return (result == ComporatorResult.Equals);
        }

       

        private void ReadVariables(out int a)
        {
            this._dataConsole.Write("Enter a:");
            a = this._dataConsole.Read();


        }

        private void Write(ComporatorResult result)
        {
            this._dataConsole.Write(result.ToString());
        }

    }
}
