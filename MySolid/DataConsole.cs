﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public class DataConsole : IDataConsole
    {
        public int Read()
        {
            return int.Parse(Console.ReadLine());
        }

        public void Write(string text)
        {
            Console.WriteLine(text);
        }
    }
}
