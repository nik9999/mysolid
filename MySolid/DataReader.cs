﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public class DataReader : IDataReader
    {
        public int Read()
        {
            return int.Parse(Console.ReadLine());
        }
    }
}
