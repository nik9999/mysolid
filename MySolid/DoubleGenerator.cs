﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public class DoubleGenerator : IGenerator<double>
    {
        public double Generate(double begin, double end)
        {
            Random rnd = new Random();
            return rnd.NextDouble() * (end-begin) ;
        }
    }
}
