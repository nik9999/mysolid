﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    //Принцип разделения интерфейса
    public interface IDataConsole : IDataWriter, IDataReader
    {
    }
}
