﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public interface IDataReader
    {
        public int Read();

    }
}
