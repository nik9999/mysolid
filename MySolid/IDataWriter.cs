﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public interface IDataWriter
    {
        public void Write(string text);

    }
}
