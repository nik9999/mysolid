﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MySolid
{
    public class IntGenerator : IGenerator<int>
    {
        // принцип единственной ответственности  SRP

        public int Generate(int begin, int end)
        {
            Random rnd= new Random();

            return rnd.Next(begin, end);
        }
    }
}
