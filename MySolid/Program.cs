﻿using System;

namespace MySolid
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var dataConsole = new DataConsole();

            var comparator = new Comparator();

            var generator = new IntGenerator();

            var controller = new Controller(dataConsole, generator, comparator);

            for (int i = 0; i < 10; i++)
                if (controller.Execute()) break;

        }
    }
}
